var app = new Vue({
    el: '#app',
    data: {
        speakers: [],
        messages: {},
        text: '',
        fileName: '',
        isModalVisible: false,
    },

    methods: {
        loadFileAsText: function (event) {
            var fileToLoad = event.target.files[0];
            this.fileName = fileToLoad.name;
            var fileReader = new FileReader();
            fileReader.onload = function (fileLoadedEvent) {
                this.text = fileLoadedEvent.target.result;
                var messages = parseMessages(this.text);
                app.speakers = Object.keys(messages);
                app.messages = messages
                if (app.speakers.length === 0) {
                    alert("We were not able to parse the uploaded file. make sure the file is an exported Whatsapp chat or contact support");
                    throw 42;
                }
            };
            fileReader.readAsText(fileToLoad, "UTF-8");
        },
        showModal() {
            this.isModalVisible = true;
        },
        hideModal() {
            this.isModalVisible = false;
        },
        play() {
            let sM = genRiddle(this.messages);
            alert(sM[1]);
            alert(sM[0]);
        }
    }
});

function parseMessages(text) {
    var TIME = /[?\d{1,2}[/.]\d{1,2}[/.]\d{2,4}, \d{1,2}:\d{2}(?::\d{1,2})?(?: (?:AM|PM))?]?(?: -)? /i;
    var SPEAKER_MESSAGE = /([^:]*): (.*)/i;
    var sender_messages = text.split(TIME);
    var messages = new Object();
    for (var i = sender_messages.length - 1; i >= 0; i--) {
        var speaker_message = sender_messages[i].match(SPEAKER_MESSAGE);
        if ((speaker_message) && (speaker_message[1] != null)) {
            let speaker = speaker_message[1];
            let message = speaker_message[2];
            if (!(speaker in messages)) {
                messages[speaker] = [];
            }
            if (message != '<Media omitted>'){
                messages[speaker].push(message);    
            }
        }
    }
    return messages;
}

function _getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function genRiddle(messages) {
    speakers = Object.keys(messages);
    let chosenOne = speakers[_getRandomInt(speakers.length)];
    let chosenMessages = messages[chosenOne];
    let chosenMessage = chosenMessages[_getRandomInt(chosenMessages.length)];
    return [chosenOne, chosenMessage];
}

